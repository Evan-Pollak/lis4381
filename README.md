# LIS4381 - Mobile Web Application Development

## Evan Pollak

### LIS4381 Requirements:

*Course Work Links*:

1. [A1 README.md](https://bitbucket.org/Evan-Pollak/lis4381/src/master/a1/README.md"A1 README.md")
  
    * Install AMPPS

    * Install JDK

    * Install Android Studio and create My First App

    * Provide screenshots of installations

    * Create Bitbucket repo

    * Complete Bitbucket tutorials (bitbucketstationlocations)

    * Provide git command descriptions

2. [A2 README.md](https://bitbucket.org/Evan-Pollak/lis4381/src/master/a2/README.md)
    
    * Create a mobile recipe app using Android Studio
    * Use Java to create second pane inside of application

3. [A3 README.md](https://bitbucket.org/Evan-Pollak/lis4381/src/master/a3/README.md)

    * Create an ERD
    * Create a mobile app that calculates concert ticket prices

4. [P1 README.md](https://bitbucket.org/Evan-Pollak/lis4381/src/master/p1/README.md)

    * Create a mobile app that acts as a business card
    * App has multiple paines and custom icon launcher

5. [A4 README.md](https://bitbucket.org/Evan-Pollak/lis4381/src/master/a4/README.md)

    * Create a mobile first web application that acts as online portfolio
    * Create a client side Boostrap validation form

6. [A5 README.md](https://bitbucket.org/Evan-Pollak/lis4381/src/master/a5/README.md)

    * Create form for user entered data that uses server-side validation
    * Use Regular Expressions with jQuery validation to verify inputs as valid
    * Link mySQL database to input data
    * Display data using a table created with html and php

7. [P2 README.md](https://bitbucket.org/Evan-Pollak/lis4381/src/master/p2/README.md)

    * Create form to update records in a database using server-side validation
    * Create server-side process for deleting records in a database
    * Create a page that displays a RSS Feed
    * Update website homepage with new pictures