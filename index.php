<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Welcome to my online portfolio! Here are some of the projects I have completed in LIS 4381.">
  <meta name="author" content="Evan Pollak">
  <link rel="icon" href="favicon.ico">

  <title>My Online Portfolio</title>

    <?php include_once("css/include_css.php"); ?> 

<!-- Carousel styles -->
<style type="text/css">
h2
{
  margin: 0;    
  color: #666;
  padding-top: 50px;
  font-size: 52px;
  font-family: "trebuchet ms", sans-serif;
}
.item
{
  background: #333;    
  text-align: center;
  height: 300px !important;
}
.carousel
{
  margin: 20px 0px 20px 0px;
}
.bs-example
{
  margin: 20px;
}
.item img {
  width:100%
}
</style>

</head>
<body>

  <?php include_once("global/nav_global.php"); ?>
  
  <div class="container">
     <div class="starter-template">
            <div class="page-header">
              <?php include_once("global/header.php"); ?> 
            </div>

<!-- Start Bootstrap Carousel  -->
<div class="bs-example">
  <div
      id="myCarousel"
    class="carousel"
    data-interval="1000"
    data-pause="hover"
    data-wrap="true"
    data-keyboard="true"      
    data-ride="carousel">
    
      <!-- Carousel indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>   
       <!-- Carousel items -->
        <div class="carousel-inner">

      <!-- -Note: you will need to modify the code to make it work with *both* text and images.  -->
            <div class="item active">
                <img src="images/java.png" alt="java" style="width:100%;">
                <div class="carousel-caption carousel-fixed">
                <h3>Assignment 1 Developement Tools</h3>
                <a href="http://localhost/repos/lis4381/a1">Java!</a>
                </div>
            </div>

            <div class="item">
                <img src="images/a3.png" alt="a3" style="width:100%;">
                <div class="carousel-caption carousel-fixed">
                <h3>Assignment 3 MySQL</h3>
                <a href="http://localhost/repos/lis4381/a3">Petstore Database</a>
                </div>
            </div>

            <div class="item">
                <img src="images/2.png" alt="bitbucket" style="width:100%;">
                <div class="carousel-caption carousel-fixed">
                <h3>Bitbucket!</h3>
                <a href="https://bitbucket.org/Evan-Pollak/lis4381/src/master/">README.md</a>
                </div>
            </div>

        </div>
        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
</div>
<!-- End Bootstrap Carousel  -->
            
<?php
include_once "global/footer.php";
?>

  </div> <!-- end starter-template -->
</div> <!-- end container -->

    <?php include_once("js/include_js.php"); ?> 
  
</body>
</html>
