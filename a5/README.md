# LIS4381 - Mobile Web Application Development

## Evan Pollak

## Assignment 5

### Assignment Requirements:
1. Create form for user entered data that uses server-side validation
2. Use Regular Expressions with jQuery validation to verify inputs as valid
3. Link mySQL database to input data
4. Display data using a table created with html and php

#### Assignment Screenshots:

![alt](img/1.png)
![alt](img/2.png)

#### [Local LIS4381 Web App](http://localhost/repos/lis4381/)
