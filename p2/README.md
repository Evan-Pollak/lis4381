# LIS4381 - Mobile Web Application Development

## Evan Pollak

## Project 2

### Assignment Requirements:
1. Create form to update records in a database using server-side validation
2. Create server-side process for deleting records in a database
3. Create a page that displays a RSS Feed
4. Update website homepage with new pictures


#### Assignment Screenshots:

![alt](img/1.png)
![alt](img/2.png)
![alt](img/3.png)
![alt](img/4.png)
![alt](img/5.png)

#### [Local LIS4381 Web App](http://localhost/repos/lis4381/)

