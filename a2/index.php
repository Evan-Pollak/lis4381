<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Assignment 2">
	<meta name="author" content="Evan Pollak">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment 2</title>
		<?php include_once("/Applications/AMPPS/www/repos/lis4381/css/include_css.php"); ?>
</head>
<body>

	<?php include_once("/Applications/AMPPS/www/repos/lis4381/global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<h1>Assignment 2</h1>	
					</div>
		
		</div> <!-- end starter-template -->

		<div class="row">
			<div class="col-md-12">
            <h3 id="assignmentrequirements">Assignment Requirements:</h3>

<ol>
<li>Create a mobile recipe app using Android Studio</li>
</ol>

<h3 id="assignmentscreenshots">Assignment Screenshots:</h3>

<p><em>Screenshot 1</em>:</p>

<p><img class="img-responsive" src="img/1.png" alt="1" /></p>

<p><em>Screenshot 2</em>:</p>

<p><img class="img-responsive" src="img/2.png" alt="2" /></p>
			</div>
		</div>
		
		<div class="starter-template"> <?php include_once "/Applications/AMPPS/www/repos/lis4381/global/footer.php"; ?> </div>
			
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("/Applications/AMPPS/www/repos/lis4381/js/include_js.php"); ?>


</body>
</html>