# LIS4381 - Mobile Web Application Development

## Evan Pollak

### Project 1

#### Assignment Requirements:
1. Create a mobile app that acts as a business card
2. Add custom launcher icon
3. Add borders around image and buttons
4. Add shadow to button text
5. Add background color to all activities

##### Assignment Screenshots:

1st Screen | 2nd Screen
- | - 
![alt](img/1.png) | ![alt](img/2.png)
