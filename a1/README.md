# LIS4381 - Mobile Web Application Development

## Evan Pollak

### Assignment 1

#### Git Commands:
1. git init - Create new repository or initilizate an existing one
2. git status - See what files need to be added from local or which ones have been deleted
3. git add - Add new/modified file from local repo
4. git commit - Creates snapshot of the repo with the new changes
5. git push - Upload local repo to remote one
6. git pull - Download remote repo content to local one
7. git clone - Create copy of existing repo

##### Assignment Screenshots:

*Screenshot of AMPPS home page*:

![Ampps Screenshot](img/ampps.png)

*Screenshot of running Hello.java*:

![Java Screenshot](img/java.png)

*Screenshot of Emulated Pixel XL running 'My first app'*:
![First App Screenshot](img/first_app.png)

#### Assignment 1 Link:

*Assignment 1:*
[A1 Repo Link](https://bitbucket.org/Evan-Pollak/lis4381/src/master/a1/)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Evan-Pollak/bitbucketstationlocations/src/master/)

*Tutorial: Request to update a teammate's reposity:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/plantevan/first-impressions/src/master/)
