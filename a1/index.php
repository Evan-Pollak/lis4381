<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Assignment 1">
	<meta name="author" content="Evan Pollak">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment1</title>
		<?php include_once("/Applications/AMPPS/www/repos/lis4381/css/include_css.php"); ?>
</head>
<body>

	<?php include_once("/Applications/AMPPS/www/repos/lis4381/global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<h1>Assignment 1</h1>	
					</div>
		
		</div> <!-- end starter-template -->

		<div class="row">
			<div class="col-md-12">
			<h3 id="gitcommands">Git Commands:</h4>

<ol>
<li>git init - Create new repository or initilizate an existing one</li>

<li>git status - See what files need to be added from local or which ones have been deleted</li>

<li>git add - Add new/modified file from local repo</li>

<li>git commit - Creates snapshot of the repo with the new changes</li>

<li>git push - Upload local repo to remote one</li>

<li>git pull - Download remote repo content to local one</li>

<li>git clone - Create copy of existing repo</li>
</ol>

<h3 id="assignmentscreenshots">Assignment Screenshots:</h5>

<p><em>Screenshot of AMPPS home page</em>:</p>

<p><img class="img-responsive" src="img/ampps.png" alt="Ampps Screenshot" /></p>

<p><em>Screenshot of running Hello.java</em>:</p>

<p><img class="img-responsive" src="img/java.png" alt="Java Screenshot" /></p>

<p><em>Screenshot of Emulated Pixel XL running 'My first app'</em>:
<img class="img-responsive" src="img/first_app.png" alt="First App Screenshot" /></p>

<h3 id="assignment1link">Assignment 1 Link:</h4>

<p><em>Assignment 1:</em>
<a href="https://bitbucket.org/Evan-Pollak/lis4381/src/master/a1/">A1 Repo Link</a></p>

<h3 id="tutoriallinks">Tutorial Links:</h4>

<p><em>Bitbucket Tutorial - Station Locations:</em>
<a href="https://bitbucket.org/Evan-Pollak/bitbucketstationlocations/src/master/">A1 Bitbucket Station Locations Tutorial Link</a></p>

<p><em>Tutorial: Request to update a teammate's reposity:</em>
<a href="https://bitbucket.org/plantevan/first-impressions/src/master/">A1 My Team Quotes Tutorial Link</a></p>
			</div>
		</div>
		
		<div class="starter-template"> <?php include_once "/Applications/AMPPS/www/repos/lis4381/global/footer.php"; ?> </div>
			
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("/Applications/AMPPS/www/repos/lis4381/js/include_js.php"); ?>


</body>
</html>
