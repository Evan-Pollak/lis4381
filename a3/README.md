# LIS4381 - Mobile Web Application Development

## Evan Pollak

### Assignment 3

#### Assignment Requirements:
1. Create ERD
2. Foward Engineer to database
3. Create a mobile app that calculates concert ticket prices

#### Links:
1. [a3.mwb](https://bitbucket.org/Evan-Pollak/lis4381/src/master/a3/a3.mwb)
2. [a3.sql](https://bitbucket.org/Evan-Pollak/lis4381/src/master/a3/a3.sql)

##### Assignment Screenshots:

*Screenshot 1*:

![a3](img/a3.png)

*Screenshot 2*:

![1](img/1.png)

*Screenshot 3*:

![2](img/2.png)
