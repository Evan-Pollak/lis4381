# LIS4381 - Mobile Web Application Development

## Evan Pollak

## Assignment 4

### Assignment Requirements:
1. Create a locally hosted web application that acts as an online portfolio
2. Create a form that uses Bootstrap client side validation
3. Port README.md's from previous assignments onto web application

#### Assignment Screenshots:

![alt](img/1.png)
![alt](img/2.png)
![alt](img/3.png)

#### [Local LIS4381 Web App](http://localhost/repos/lis4381/)
